{ config, pkgs, lib, ... }:
{
  i18n.supportedLocales = [ "ru_RU.UTF-8/UTF-8" ];

  nix.buildMachines = [
    { hostName = "localhost";
      systems = [ "aarch64-linux" "armv7l-linux" ] ;
      supportedFeatures = [ "nixos-test" "big-parallel" "benchmark" ];
      maxJobs = 4;
      protocol = null;
    }
  ];

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.hydra = {
    enable = true;
    hydraURL = "https://hydra.armv7l.xyz"; # externally visible URL
    notificationSender = "hydra@localhost"; # e-mail of hydra service
    # a standalone hydra will require you to unset the buildMachinesFiles list to avoid using a nonexistant /etc/nix/machines
    # buildMachinesFiles = [];
    # you will probably also want, otherwise *everything* will be built from scratch
    useSubstitutes = true;
    listenHost = "127.0.0.1";
    extraConfig = ''
      server_store_uri = https://cache.armv7l.xyz
    '';
  };
  # https://github.com/NixOS/hydra/issues/1186#issuecomment-1231513076
  systemd.services.hydra-evaluator.environment.GC_DONT_GC = "true";

  services.nginx = {
    enable = true;
    virtualHosts = {
      "hydra.armv7l.xyz" = {
        enableACME = true;
        forceSSL = true;
        locations = {
          "/" = {
            proxyPass = "http://${config.services.hydra.listenHost}:${toString config.services.hydra.port}";
          };
        };
      };
    };
  };
}
