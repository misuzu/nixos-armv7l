{ config, ... }:
{
  imports = [
    ./atticd.nix
    ./attic-upload.nix
    ./module.nix
  ];
}
