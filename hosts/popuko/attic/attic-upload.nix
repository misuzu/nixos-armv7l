{ config, ... }:
{
  age.secrets."cache-armv7l-xyz-conf.toml".file = ./cache-armv7l-xyz-conf.toml.age;

  services.attic-upload = {
    enable = true;
    atticConfigFile = config.age.secrets."cache-armv7l-xyz-conf.toml".path;
    cache = "xyz-armv7l:xyz-armv7l-cache";
  };
}
