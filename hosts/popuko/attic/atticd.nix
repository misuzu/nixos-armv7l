{ config, pkgs, ... }:
{
  age.secrets."atticd.env".file = ./atticd.env.age;

  nix = {
    gc = {
      automatic = true;
      dates = "daily";
    };
    settings = {
      substituters = [
        "https://cache.armv7l.xyz"
      ];
      trusted-public-keys = [
        "cache.armv7l.xyz-1:kBY/eGnBAYiqYfg0fy0inWhshUo+pGFM3Pj7kIkmlBk="
      ];
      builders-use-substitutes = true;
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  networking.hosts = {
    "127.0.0.1" = [ "cache.armv7l.xyz" ];
  };

  services.nginx = {
    enable = true;
    enableReload = true;
    virtualHosts = {
      "cache.armv7l.xyz" = {
        enableACME = true;
        addSSL = true;
        locations = {
          " = /" = {
            extraConfig = "return 302 https://hydra.armv7l.xyz;";
          };
          "/" = {
            proxyPass = "http://127.0.0.1:8080/xyz-armv7l-cache/";
          };
        };
        extraConfig = ''
          access_log off;
          error_log /dev/null;
          # error_log stderr;
          add_header Cache-Control "public, immutable, max-age=86400";
        '';
      };
    };
  };

  services.atticd = {
    enable = true;
    package = pkgs.attic-server;
    useFlakeCompatOverlay = false;
    credentialsFile = config.age.secrets."atticd.env".path;
    settings = {
      listen = "[::]:8080";
      chunking = {
        # The minimum NAR size to trigger chunking
        #
        # If 0, chunking is disabled entirely for newly-uploaded NARs.
        # If 1, all NARs are chunked.
        nar-size-threshold = 128 * 1024; # 128 KiB

        # The preferred minimum size of a chunk, in bytes
        min-size = 64 * 1024; # 64 KiB

        # The preferred average size of a chunk, in bytes
        avg-size = 128 * 1024; # 128 KiB

        # The preferred maximum size of a chunk, in bytes
        max-size = 256 * 1024; # 256 KiB
      };
      storage = {
        type = "local";
        path = "/var/lib/attic";
      };
    };
  };
}
