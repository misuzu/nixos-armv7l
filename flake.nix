{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  inputs.agenix.url = "github:ryantm/agenix";
  inputs.agenix.inputs.nixpkgs.follows = "nixpkgs";

  inputs.agenix-cli.url = "github:cole-h/agenix-cli";
  inputs.agenix-cli.inputs.nixpkgs.follows = "nixpkgs";

  inputs.attic.url = "github:zhaofengli/attic";
  inputs.attic.inputs.nixpkgs.follows = "nixpkgs";

  outputs = inputs: 
  let
    domain = "armv7l.xyz";
    machines = [
      {
        host = "popuko";
        system = "aarch64-linux";
        extraModules = [
          ./common/nginx
          ./common/slim.nix
          ./common/zfs.nix
        ];
      }
    ];
  in {
    devShells = inputs.nixpkgs.lib.genAttrs
      [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ]
      (system:
        let
          pkgs = import inputs.nixpkgs { inherit system; };
        in
        {
          default = pkgs.mkShell {
            nativeBuildInputs = [
              inputs.agenix-cli.defaultPackage.${system}
            ];
          };
        });

    nixosConfigurations = with inputs.nixpkgs.lib; listToAttrs (map
      ({ host
       , name ? host
       , configuration ? "configuration.nix"
       , system ? "x86_64-linux"
       , nixpkgs ? inputs.nixpkgs
       , extraModules ? [ ]
       , ... }: nameValuePair name (
        nixpkgs.lib.nixosSystem {
          inherit system;

          specialArgs = {
            inherit inputs;
          };

          modules = [
            inputs.agenix.nixosModules.age

            (inputs.attic + "/nixos/atticd.nix")

            ./common/overlays

            ./common/acme.nix
            ./common/coredump.nix
            ./common/dhcpcd.nix
            ./common/firewall.nix
            ./common/flakes.nix
            ./common/i18n.nix
            ./common/journald.nix
            ./common/nix.nix
            ./common/openssh.nix
            ./common/sysctl.nix
            ./common/tmp.nix
            ./common/users.nix
            ./common/zramSwap.nix
          ] ++ nixpkgs.lib.optional (configuration != null)
              # Use host configuration
              ./hosts/${host}/${configuration}
            ++ extraModules;
        })
      )
      machines
    );
  };
}
