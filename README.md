# NixOS configuration


## Install

```bash
# https://nixos.org/manual/nixos/stable/index.html#sec-installation
mkdir -p /mnt/etc/nixos/
cd /mnt/etc/nixos/
nix-shell -p git
git clone https://gitlab.com/misuzu/nixos-armv7l.git nixos-configuration
cd nixos-configuration
# Generate new hardware-configuration.nix
nixos-generate-config --root /mnt --show-hardware-config > hosts/<hostname>/hardware-configuration.nix
# Install system
nixos-install --root /mnt --impure --no-channel-copy --flake .#<hostname>
# Set password for `misuzu` user
nixos-enter --root /mnt -c 'passwd misuzu'
# Reboot
reboot
```

## Apply configuration

```bash
nixos-rebuild switch --use-remote-sudo --fast --flake .
```


## Upgrade

```bash
nix flake update --commit-lock-file
nixos-rebuild boot --use-remote-sudo --fast --flake .
sudo reboot
```


## Rollback upgrade

```bash
git revert HEAD
nixos-rebuild boot --use-remote-sudo --fast --flake .
sudo reboot
```


## Configuration options

Packages search - https://search.nixos.org/packages

NixOS - https://search.nixos.org/options

home-manager - https://nix-community.github.io/home-manager/options.html
