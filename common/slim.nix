{ config, lib, pkgs, ... }:
{
  documentation.enable = lib.mkDefault false;
  documentation.nixos.enable = lib.mkDefault false;
  programs.bash.completion.enable = lib.mkDefault false;
  programs.command-not-found.enable = lib.mkDefault false;
  security.polkit.enable = lib.mkDefault false;
  security.rtkit.enable = lib.mkForce false;
  services.udisks2.enable = lib.mkDefault false;

  fonts.fontconfig.enable = false;

  environment.defaultPackages = [ ];
  # environment.systemPackages = lib.mkForce [ ];

  i18n.supportedLocales = [ (config.i18n.defaultLocale + "/UTF-8") ];
}
