{ config, lib, ... }:
{
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "bakalolka@gmail.com";
      enableDebugLogs = false;
    };
  };
}
