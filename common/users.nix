{ config, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users = {
    misuzu = {
      isNormalUser = true;
      uid = 1000;
      extraGroups = [
        "adbusers"
        "audio"
        "docker"
        "lp"
        "plugdev"
        "scanner"
        "video"
        "wheel" # Enable ‘sudo’ for the user.
      ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH/k1reOfT7csrtHnbp9ti+oyBlY8sS4DEeRmhJRPFJe misuzu@naomi"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILPxq3an5irer/8bjJFK0ZzXbOExSp/T7DNsLx/xtMBj misuzu@erika"
      ];
    };
    root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILQpt9BdNA+Mz19Ncq49Jl41iyJSwrbkO85jwbvaL6vT deploy@gitlab-ci"
      ] ++ config.users.users.misuzu.openssh.authorizedKeys.keys;
    };
  };

  # Disble password promt
  security.sudo.wheelNeedsPassword = false;
}
