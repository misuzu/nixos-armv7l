{ config, pkgs, ... }:
{
  services.nginx = {
    appendHttpConfig = ''
      log_format with_hosts '[$time_local] $remote_addr $host $request_time '
                            '"$request" $status $body_bytes_sent '
                            '"$http_user_agent" "$http_referer"';
      access_log syslog:server=unix:/dev/log with_hosts;
      error_log stderr;
    '';
  };
}
