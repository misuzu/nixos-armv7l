{ config, pkgs, ... }:
{
  imports = [
    ./logging.nix
    ./vts.nix
  ];
  services.nginx = {
    enableReload = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    serverTokens = false;
    appendHttpConfig = "include ${./cloudflare.conf};";
    virtualHosts = {
      "_" = {
        default = true;
        root = "${pkgs.nginx}/html";
        extraConfig = ''
          access_log off;
          error_log /dev/null;
        '';
      };
    };
  };
}
