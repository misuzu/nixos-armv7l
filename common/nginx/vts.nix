{ config, pkgs, ... }:
{
  services.nginx = {
    additionalModules = with pkgs.nginxModules; [ vts ];
    appendHttpConfig = "vhost_traffic_status_zone;";
    virtualHosts."127.0.0.1".locations."/status".extraConfig = ''
      access_log off;
      allow 127.0.0.1;
      deny all;
      vhost_traffic_status_display;
      vhost_traffic_status_display_format html;
    '';
  };
}
