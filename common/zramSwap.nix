{ config, pkgs, lib, ... }:
{
  zramSwap = {
    enable = true;
    algorithm = "lz4";
  };
}
