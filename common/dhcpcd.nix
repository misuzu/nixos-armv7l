{ config, pkgs, ... }:
{
  networking.dhcpcd.extraConfig = "noarp";
}
