{ config, pkgs, lib, ... }:
{
  boot.tmpOnTmpfs = with lib; mkIf (!hasAttr "/tmp" config.fileSystems) true;
}
