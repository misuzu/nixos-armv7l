{ config, pkgs, lib, ... }:
{
  services.openssh = {
    enable = true;
    settings = {
      KbdInteractiveAuthentication = false;
      PasswordAuthentication = lib.mkDefault false;
      Ciphers = [ "chacha20-poly1305@openssh.com" ];
      KexAlgorithms = [ "curve25519-sha256" ];
    };
  };
}
