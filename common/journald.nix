{ config, pkgs, lib, ... }:
{
  services.journald.extraConfig = ''
    SystemMaxUse=256M
  '';
}
