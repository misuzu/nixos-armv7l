{ config, pkgs, lib, ... }:
{
  boot.initrd.supportedFilesystems = [ "zfs" ];
  boot.supportedFilesystems = [ "zfs" ];

  services.zfs = {
    autoScrub = {
      enable = true;
      interval = "Sun, 04:00";
    };
    trim = {
      enable = true;
      interval = "Sat, 08:00";
    };
  };

  services.zrepl = {
    enable = true;
    settings = {
      jobs = [
        {
          name = "local";
          type = "snap";
          filesystems = {
            "${config.networking.hostName}/safe<" = true;
          };
          snapshotting = {
            type = "periodic";
            prefix = "zrepl_";
            interval = "10m";
          };
          pruning = {
            keep = [
              {
                type = "grid";
                grid = "1x1h(keep=all) | 24x1h | 30x1d | 6x30d";
                regex = "^zrepl_";
              }
              {
                type = "regex";
                negate = true;
                regex = "^zrepl_";
              }
            ];
          };
        }
      ];
    };
  };
}
