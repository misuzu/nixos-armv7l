{ config, lib, pkgs, ... }:
{
  networking.firewall = with lib; {
    enable = mkDefault true;
    allowPing = mkDefault true;
    rejectPackets = mkDefault true;
    logRefusedConnections = mkDefault false;
  };
  systemd.services.firewall.reloadIfChanged = lib.mkForce false;
}
