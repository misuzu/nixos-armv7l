{ config, pkgs, lib, inputs, ... }: {
  environment.etc."nix/channels/nixpkgs".source = inputs.nixpkgs.outPath;

  nix.nixPath = lib.mkForce [
    "nixpkgs=/etc/nix/channels/nixpkgs"
  ];
  nix.registry.nixpkgs.flake = inputs.nixpkgs;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
